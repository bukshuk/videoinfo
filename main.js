const { app, BrowserWindow, ipcMain, Menu } = require("electron");
const ffmpeg = require("fluent-ffmpeg");
const path = require("path");

let mainWindow;
let addWindow;

app.on("ready", () => {
  mainWindow = new BrowserWindow({
    title: "Video File Information",
    width: 800,
    height: 600,
    webPreferences: {
      sandbox: true,
      preload: path.join(__dirname, "preload.js"),
    },
  });
  mainWindow.loadURL(`file://${__dirname}/main.html`);
  mainWindow.on("closed", () => app.quit());

  const mainMenu = Menu.buildFromTemplate(menuTemplate);
  mainWindow.setMenu(mainMenu);
  //Menu.setApplicationMenu(mainMenu);
});

function createAddWindow() {
  addWindow = new BrowserWindow({
    title: "Add New Todo",
    width: 300,
    height: 220,
    webPreferences: {
      sandbox: true,
      preload: path.join(__dirname, "preload.js"),
    },
  });
  addWindow.loadURL(`file://${__dirname}/add.html`);
  addWindow.on("closed", () => (addWindow = null));
}

const menuTemplate = [
  {
    label: "File",
    submenu: [
      {
        label: "New Todo",
        click() {
          createAddWindow();
        },
      },
      {
        label: "Quit",
        accelerator: "Ctrl+Q",
        click() {
          app.quit();
        },
      },
    ],
  },
];

ipcMain.on("video:submit", (_, path) => {
  ffmpeg.ffprobe(path, (_, metadata) => {
    mainWindow.webContents.send("video:metadata", metadata.format.duration);
  });
});

ipcMain.on("todo:add", (_, todo) => {
  mainWindow.webContents.send("todo:add", todo);
  addWindow.close();
});

if (process.env.NODE_ENV !== "production") {
  menuTemplate.push({
    label: "Debugging",
    submenu: [
      { role: "reload" },
      {
        label: "Toggle Developer Tools",
        accelerator: "Ctrl+Shift+I",
        click() {
          mainWindow.toggleDevTools();
        },
      },
    ],
  });
}
