const { ipcRenderer, contextBridge } = require("electron");

contextBridge.exposeInMainWorld("api", {
  sendPath: (path) => ipcRenderer.send("video:submit", path),
  sendTodo: (todo) => ipcRenderer.send("todo:add", todo),
  receiveDuration: (func) => ipcRenderer.on("video:metadata", (_, duration) => func(duration)),
  receiveTodo: (func) => ipcRenderer.on("todo:add", (_, todo) => func(todo)),
});
